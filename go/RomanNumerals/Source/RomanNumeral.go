package RomanNumeral

type number struct {
	decimal int
	numeral string
}

func (n *number) add(addend number) {
	n.decimal += addend.decimal
	n.numeral += addend.numeral
}

func subtract(a number, b number) number {
	// b.numeral + a.numeral is intentional as in roman numbers when we subtract
	// one number from the other we append both strings with the number we want
	// to subtract coming first
	return number{a.decimal - b.decimal, b.numeral + a.numeral }
}

var numbers []number

func init() {
	numbers = make([]number, 0, 7)
	numbers = append(numbers, number{1, "I"})
	numbers = append(numbers, number{5, "V"})
	numbers = append(numbers, number{10, "X"})
	numbers = append(numbers, number{50, "L"})
	numbers = append(numbers, number{100, "C"})
	numbers = append(numbers, number{500, "D"})
	numbers = append(numbers, number{1000, "M"})
}

func getNumberFromDecimal(decimal int) *number {
	for i := 0; i < len(numbers); i++ {
		if decimal == numbers[i].decimal {
			return &numbers[i]
		}
	}
	return nil
}

func getNumberFromIndex(index int) *number {
	if index >= 0 && index < len(numbers) {
		return &numbers[index]
	}
	return nil
}

func findRangeIndicesForDecimal(decimal int) (int, int) {
	count := len(numbers)
	low := count - 1
	high := -1

	for i := 0; i < count; i++ {
		if decimal < numbers[i].decimal {
			low = i - 1
			high = i
			break
		}
	}

	return low, high
}

func mapDecimalToRangeOfNumbers(decimal int) (*number, *number) {
	low, high := findRangeIndicesForDecimal(decimal)
	return getNumberFromIndex(low), getNumberFromIndex(high)
}

func getPotentialNumberToSubtract(decimal int) *number {
	if decimal <= 10 {
		return getNumberFromDecimal(1)
	} else if decimal <= 100 {
		return getNumberFromDecimal(10)
	} else {
		return getNumberFromDecimal(100)
	}
}

func shouldUseSubtractNotation(decimal int, highDecimal int, subtrahendDecimal int) bool {
	return highDecimal - decimal <= subtrahendDecimal
}

func findNumberToAdd(decimal int) number {
	low, high := mapDecimalToRangeOfNumbers(decimal)
	addend := *low

	subtrahend := getPotentialNumberToSubtract(decimal)
	if high != nil && shouldUseSubtractNotation(decimal, high.decimal, subtrahend.decimal) {
		addend = subtract(*high, *subtrahend)
	}

	return addend
}

func FromDecimal(decimal int) string {
	var result number

	for result.decimal < decimal {
		result.add(findNumberToAdd(decimal - result.decimal))
	}

	return result.numeral
}